import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Register() {

const [email, setEmail]=useState('');
const [password1, setPassword1]=useState('');
const [password2, setPassword2]=useState('');
const [isActive, setIsActive]=useState(false);
// console.log(email);
// console.log(password1);
// console.log(password2);

	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering');
	}


	//hook
	useEffect(()=>{
		//validation on submit button
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[email, password1, password2]);

	return(
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= { e => setEmail(e.target.value)}
				/>
				<Form.Text>
					We'll never share your email to anyone
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value= {password1}
					onChange= { e => setPassword1(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Input your password again"
					required
					value= {password2}
					onChange= { e => setPassword2(e.target.value)}
				/>
			</Form.Group>
		{/*ternary operators { ? : } */}
		{ isActive ?
			<Button className="mt-3 mb-3" variant="success" type="submit" id="submitBtn"> 
				Register
			</Button>
			:
			<Button className="mt-3 mb-3" variant="danger" type="submit" id="submitBtn" disabled> 
				Register
			</Button>
		}
		</Form>
		</>
	)
}
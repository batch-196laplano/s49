import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Login(){

	const [email, setEmail]=useState('');
const [password, setPassword]=useState('');
const [isActive, setIsActive]=useState(false);
// console.log(email);
// console.log(password);

	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword('');

		alert('Login Successful');
	}

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[email, password]);

	return(
		<>
		<h3>Login:</h3>
		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= { e => setEmail(e.target.value)}
				/>

			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="password"
					required
					value= {password}
					onChange= { e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{/*ternary operators { ? : } */}
		{ isActive ?
			<Button className="mt-3 mb-3" variant="success" type="submit" id="submitBtn"> 
				Login
			</Button>
			:
			<Button className="mt-3 mb-3" variant="danger" type="submit" id="submitBtn" disabled> 
				Login
			</Button>
		}
		</Form>
		</>
	)
}